class MakesMeetingMaxCandidatesNumberNotNull < ActiveRecord::Migration[5.0]
  def up
    change_column :meetings, :max_candidates_number, :integer, null: false, default: 1
  end

  def down
    change_column :meetings, :max_candidates_number, :integer, null: true
  end
end
