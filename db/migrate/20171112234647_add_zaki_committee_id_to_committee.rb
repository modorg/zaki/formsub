class AddZakiCommitteeIdToCommittee < ActiveRecord::Migration[5.0]
  def change
    add_column :committees, :zaki_committee_id, :string
  end
end
