class AddMaxCandidatesNumberFields < ActiveRecord::Migration[5.0]
  def change
    add_column :committees, :minutes_per_candidate, :integer, default: 30
    add_column :meetings, :max_candidates_number, :integer
    add_column :registrations, :priority_number, :integer

    ActiveRecord::Base.transaction do
      Meeting.all.each do |meeting|
        meeting.registrations.order(:id).each_with_index do |registration, index|
          registration.update!(priority_number: index)
        end
      end
    end
  end
end
