class AddCaseIdToRegistration < ActiveRecord::Migration[5.0]
  def change
    add_reference :registrations, :case, foreign_key: true, index: true
  end
end
