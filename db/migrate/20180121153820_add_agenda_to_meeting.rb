class AddAgendaToMeeting < ActiveRecord::Migration[5.0]
  def change
    add_column :meetings, :agenda, :text
  end
end
