class AddRegistrationsCountToMeeting < ActiveRecord::Migration[5.0]
  def change
    add_column :meetings, :registrations_count, :integer, default: 0
  end
end
