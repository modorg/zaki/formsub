class CreateMeetings < ActiveRecord::Migration[5.0]
  def change
    create_table :meetings do |t|
      t.string :name, null: false
      t.belongs_to :committee, foreign_key: true, null: false
      t.datetime :time_start
      t.datetime :time_end
      t.boolean :approved

      t.timestamps
    end
  end
end
