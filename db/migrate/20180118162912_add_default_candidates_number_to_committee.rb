class AddDefaultCandidatesNumberToCommittee < ActiveRecord::Migration[5.0]
  def change
    add_column :committees, :default_candidates_number, :integer, default: 2
  end
end
