class CreateRegistrations < ActiveRecord::Migration[5.0]
  def change
    create_table :registrations do |t|
      t.string :title, null: false
      t.text :description
      t.belongs_to :meeting, foreign_key: true, null: false

      t.timestamps
    end
  end
end
