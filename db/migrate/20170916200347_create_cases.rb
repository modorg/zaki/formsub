class CreateCases < ActiveRecord::Migration[5.0]
  def change
    create_table :cases do |t|
      t.string :title, null: false
      t.string :keyword, null: false

      t.timestamps
    end
    add_index :cases, :keyword, unique: true
  end
end
