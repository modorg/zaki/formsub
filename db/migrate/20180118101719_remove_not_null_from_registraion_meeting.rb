class RemoveNotNullFromRegistraionMeeting < ActiveRecord::Migration[5.0]
  def change
    change_column :registrations, :meeting_id, :integer, :null => true
  end
end
