class ResetCounterCachesMeetingRegistrations < ActiveRecord::Migration[5.0]
  def up
    Meeting.all.pluck(:id).each { |meeting_id| Meeting.reset_counters(meeting_id, :registrations) }
  end

  def down; end
end
