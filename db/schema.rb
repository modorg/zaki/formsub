# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20181129095214) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "cases", force: :cascade do |t|
    t.string   "title",      null: false
    t.string   "keyword",    null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["keyword"], name: "index_cases_on_keyword", unique: true, using: :btree
  end

  create_table "committees", force: :cascade do |t|
    t.string   "name",                                   null: false
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "zaki_committee_id"
    t.integer  "minutes_per_candidate",     default: 30
    t.integer  "default_candidates_number", default: 2
    t.index ["name"], name: "index_committees_on_name", unique: true, using: :btree
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
    t.index ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
    t.index ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree
  end

  create_table "meetings", force: :cascade do |t|
    t.string   "name",                              null: false
    t.integer  "committee_id",                      null: false
    t.datetime "time_start"
    t.datetime "time_end"
    t.boolean  "approved"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.integer  "max_candidates_number", default: 1, null: false
    t.text     "agenda"
    t.integer  "registrations_count",   default: 0
    t.index ["committee_id"], name: "index_meetings_on_committee_id", using: :btree
  end

  create_table "registrations", force: :cascade do |t|
    t.string   "title",           null: false
    t.text     "description"
    t.integer  "meeting_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "case_id"
    t.integer  "priority_number"
    t.index ["case_id"], name: "index_registrations_on_case_id", using: :btree
    t.index ["meeting_id"], name: "index_registrations_on_meeting_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "uid"
    t.string   "email"
    t.string   "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "meetings", "committees"
  add_foreign_key "registrations", "cases"
  add_foreign_key "registrations", "meetings"
end
