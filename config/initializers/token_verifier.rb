class TokenVerifier
  # this class should handle Forbidden error during post login request
  include ActiveSupport::Configurable
  include ActionController::RequestForgeryProtection

  def call(env)
    @request = ActionDispatch::Request.new(env.dup)
    raise OmniAuth::AuthenticityError unless verified_request?
  end

  private
  attr_reader :request
  delegate :params, :session, to: :request
end
# in an initializer
OmniAuth.config.request_validation_phase = TokenVerifier.new
