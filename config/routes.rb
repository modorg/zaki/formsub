# frozen_string_literal: true
Rails.application.routes.draw do
  resources :cases

  # resources :registrations
  resources :committees do
    resources :registrations, only: :index
    resources :meetings do
      match :edit_agenda, via: :get
      match :generate_agenda, via: :get

      resources :registrations
      match :increment_max_candidates_number, via: :get
      match :decrement_max_candidates_number, via: :get
      match :approve, via: :get
    end
  end

  root 'home#index'
  match 'home/private_index' => 'home#private_index', via: :get

  match 'get_permitted_keywords' => 'home#permitted_keywords_get', via: :get
  match 'set_permitted_keywords' => 'home#permitted_keywords_set', via: :get

  # omniauth
  get '/auth/:provider/callback' => 'user_sessions#create'
  get '/auth/failure' => 'user_sessions#failure'
  get 'auth/autologin' => 'user_sessions#autologin'

  # Custom logout
  match '/logout', to: 'user_sessions#destroy', via: :all
end
