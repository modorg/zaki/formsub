# frozen_string_literal: true
require 'rails_helper'

RSpec.describe Meeting, type: :model do
  let(:meeting) { create(:meeting) }
  let(:past_meeting) { create(:meeting, :past) }

  describe 'validations' do
    before { allow(subject).to receive(:set_max_candidates_number) { nil } }

    it { is_expected.to validate_presence_of :name }
    it { is_expected.to validate_presence_of :committee }
    it { is_expected.to validate_presence_of :time_start }
    it { is_expected.to validate_presence_of :time_end }
    it { is_expected.to validate_numericality_of(:max_candidates_number).is_greater_than(0) }
    it { is_expected.to belong_to(:committee) }
    it { is_expected.to have_many(:registrations) }
  end

  describe 'max_candidates_number' do
    subject do
      build(:meeting, time_start: 3.hours.from_now, time_end: 5.hours.from_now,
                      committee: create(:committee), max_candidates_number: nil)
    end

    it { is_expected.to have_attributes(max_candidates_number: nil) }

    context 'after validation' do
      before { subject.save }

      it { is_expected.to have_attributes(max_candidates_number: 4) }
    end
  end

  describe '#time_end_is_after_start validation' do
    let(:meeting) { build(:meeting, time_end: 1.day.from_now - 2.hours) }
    subject { meeting }

    it { is_expected.not_to be_valid }
  end

  describe '#registration_possible? meeting.max_candidates_number is 4 by factory default' do
    subject { meeting.registration_possible? }
    it { is_expected.to be true }

    context 'future meeting' do
      context 'with full registrations' do
        before { allow(meeting).to receive_message_chain('registrations.size') { 4 } }
        it { is_expected.to be false }
      end

      context 'with registrations slots available' do
        before { allow(meeting).to receive_message_chain('registrations.size') { 3 } }
        it { is_expected.to be true }
      end
    end

    context 'past meeting' do
      subject { past_meeting.registration_possible? }

      context 'with full registrations' do
        before { allow(meeting).to receive_message_chain('registrations.size') { 4 } }
        it { is_expected.to be false }
      end

      context 'with registrations slots available' do
        before { allow(meeting).to receive_message_chain('registrations.size') { 3 } }
        it { is_expected.to be false }
      end
    end
  end

  describe 'max_candidates_number changes' do
    let(:meeting) { create(:meeting, max_candidates_number: 3) }
    it 'increment_max_candidates_number changes value to up' do
      expect { meeting.increment_max_candidates_number }.to \
        change { meeting.max_candidates_number }.by(1)
    end

    it 'decrement_max_candidates_number changes value to down' do
      expect { meeting.decrement_max_candidates_number }.to \
        change { meeting.max_candidates_number }.by(-1)
    end
  end

  describe '#approve!' do
    let(:meeting) { create(:meeting, approved: false) }
    it { expect { meeting.approve! }.to change { meeting.approved }.from(false).to(true) }
  end

  describe '#future?' do
    context 'future meeting' do
      subject { meeting.future? }

      it { is_expected.to be_truthy }
    end

    context 'past meeting' do
      subject { past_meeting.future? }

      it { is_expected.to be_falsey }
    end
  end

  describe 'scopes' do
    let(:committee) { create(:committee) }
    let!(:meeting1) { create(:meeting, name: 'approved reg', committee: committee, approved: true) }
    let!(:meeting2) { create(:meeting, name: 'unapproved', committee: committee, approved: false) }
    let!(:meeting3) { create(:meeting, :past, name: 'pstap', committee: committee, approved: true) }
    let!(:meeting4) { create(:meeting, :past, name: 'pstu', committee: committee, approved: false) }
    let!(:meeting5) { create(:meeting, name: 'mx', committee: committee, max_candidates_number: 4) }

    before do
      4.times { create(:registration, meeting: meeting5) }
      2.times { create(:registration, meeting: meeting1) }
    end

    it { expect(described_class.approved).to match_array([meeting1, meeting3]) }
    it { expect(described_class.unapproved).to match_array([meeting2, meeting4, meeting5]) }
    it { expect(described_class.full).to eq([meeting5]) }
    it { expect(described_class.free).to match_array([meeting1, meeting2, meeting3, meeting4]) }
    it { expect(described_class.empty).to match_array([meeting2, meeting3, meeting4]) }
    it { expect(described_class.future).to match_array([meeting1, meeting2, meeting5]) }
    it { expect(described_class.past).to match_array([meeting3, meeting4]) }
  end
end
