# frozen_string_literal: true
require 'rails_helper'

RSpec.describe Committee, type: :model do
  it { is_expected.to validate_presence_of :name }
  # it { is_expected.to validate_uniqueness_of :name }
  it { is_expected.to validate_presence_of :zaki_committee_id }
  it { is_expected.to validate_presence_of :minutes_per_candidate }
  it { is_expected.to validate_numericality_of :minutes_per_candidate }

  it { is_expected.to have_many(:meetings) }
  it { is_expected.to have_many(:registrations) }
end
