# frozen_string_literal: true
require 'rails_helper'

RSpec.describe Registration, type: :model do
  describe 'validations' do
    before { allow(subject).to receive(:set_priority_number) { nil } }

    it { is_expected.to validate_presence_of :title }
    it { is_expected.to validate_presence_of :meeting }
    it { is_expected.to validate_presence_of :case }
    it { is_expected.to belong_to(:meeting) }
  end

  it { is_expected.to belong_to :case }
end
