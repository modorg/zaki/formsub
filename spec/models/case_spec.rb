# frozen_string_literal: true
require 'rails_helper'

RSpec.describe Case, type: :model do
  let(:cse) { create(:case) }

  it { is_expected.to validate_presence_of(:title) }
  it { is_expected.to validate_presence_of(:keyword) }

  # it { is_expected.to validate_uniqueness_of(:title) }
  it { is_expected.to allow_value('xxx').for(:keyword) }
  it { expect(cse).not_to allow_value('xxx').for(:keyword) }

  it { is_expected.to have_many :registrations }

  describe '#add_id_to_keyword' do
    it 'calls add_id_to_keyword after create' do
      cse = Case.new(attributes_for(:case))
      expect(cse).to receive(:add_id_to_keyword)
      cse.save
    end
  end
end
