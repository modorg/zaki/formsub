# frozen_string_literal: true

require 'rails_helper'

FactoryGirl.define do
  factory :case do
    sequence :title do |n|
      "case title#{n}"
    end
    sequence :keyword do |n|
      "case keyword#{n}"
    end
  end
end
