# frozen_string_literal: true

require 'rails_helper'

FactoryGirl.define do
  factory :meeting do
    name            'some meeting name'
    association     :committee
    time_start      1.day.from_now
    time_end        1.day.from_now + 2.hours
    approved        false
    max_candidates_number 1

    trait :past do
      time_start  1.day.ago - 2.hours
      time_end    1.day.ago
    end
  end
end
