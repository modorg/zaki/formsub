# frozen_string_literal: true

require 'rails_helper'

FactoryGirl.define do
  factory :registration do
    sequence :title do |n|
      "registration name#{n}"
    end

    description     'info for committee'
    association     :meeting
    association     :case
  end
end
