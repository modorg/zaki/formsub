# frozen_string_literal: true

require 'rails_helper'

FactoryGirl.define do
  factory :committee do
    sequence :name do |n|
      "committee name#{n}"
    end

    sequence :zaki_committee_id do |n|
      n
    end
  end
end
