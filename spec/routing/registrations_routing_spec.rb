# frozen_string_literal: true
require 'rails_helper'

RSpec.describe RegistrationsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/committees/1/meetings/1/registrations').to \
        route_to('registrations#index', committee_id: '1', meeting_id: '1')
    end

    it 'routes to #new' do
      expect(get: '/committees/1/meetings/1/registrations/new').to \
        route_to('registrations#new', committee_id: '1', meeting_id: '1')
    end

    it 'routes to #show' do
      expect(get: '/committees/1/meetings/1/registrations/1').to \
        route_to('registrations#show', id: '1', committee_id: '1', meeting_id: '1')
    end

    it 'routes to #edit' do
      expect(get: '/committees/1/meetings/1/registrations/1/edit').to \
        route_to('registrations#edit', id: '1', committee_id: '1', meeting_id: '1')
    end

    it 'routes to #create' do
      expect(post: '/committees/1/meetings/1/registrations').to \
        route_to('registrations#create', committee_id: '1', meeting_id: '1')
    end

    it 'routes to #update via PUT' do
      expect(put: '/committees/1/meetings/1/registrations/1').to \
        route_to('registrations#update', id: '1', committee_id: '1', meeting_id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/committees/1/meetings/1/registrations/1').to \
        route_to('registrations#update', id: '1', committee_id: '1', meeting_id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/committees/1/meetings/1/registrations/1').to \
        route_to('registrations#destroy', id: '1', committee_id: '1', meeting_id: '1')
    end
  end
end
