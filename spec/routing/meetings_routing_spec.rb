# frozen_string_literal: true
require 'rails_helper'

RSpec.describe MeetingsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/committees/1/meetings').to route_to('meetings#index', committee_id: '1')
    end

    it 'routes to #new' do
      expect(get: '/committees/1/meetings/new').to route_to('meetings#new', committee_id: '1')
    end

    it 'routes to #show' do
      expect(get: '/committees/1/meetings/1').to route_to('meetings#show', committee_id: '1',
                                                                           id: '1')
    end

    it 'routes to #edit' do
      expect(get: '/committees/1/meetings/1/edit').to route_to('meetings#edit', committee_id: '1',
                                                                                id: '1')
    end

    it 'routes to #create' do
      expect(post: '/committees/1/meetings').to route_to('meetings#create', committee_id: '1')
    end

    it 'routes to #update via PUT' do
      expect(put: '/committees/1/meetings/1').to route_to('meetings#update', committee_id: '1',
                                                                             id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/committees/1/meetings/1').to route_to('meetings#update', committee_id: '1',
                                                                               id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/committees/1/meetings/1').to route_to('meetings#destroy', committee_id: '1',
                                                                                 id: '1')
    end
  end
end
