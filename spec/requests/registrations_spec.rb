# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'Registrations', type: :request do
  describe 'GET /registrations' do
    let(:meeting) { create(:meeting) }
    before { allow_any_instance_of(ApplicationHelper).to receive(:current_user) { create(:user) } }

    it 'works! (now write some real specs)' do
      get committee_meeting_registrations_path(committee_id: meeting.committee.id,
                                               meeting_id: meeting.id)
      expect(response).to have_http_status(200)
    end
  end
end
