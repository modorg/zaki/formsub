# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'Meetings', type: :request do
  describe 'GET /meetings' do
    let(:committee) { create(:committee) }
    it 'works! (now write some real specs)' do
      get committee_meetings_path(committee_id: committee.id)
      expect(response).to have_http_status(302)
    end
  end
end
