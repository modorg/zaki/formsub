# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'registrations/new', type: :view do
  before(:each) do
    @registration = assign(:registration, build(:registration))
    @meeting = assign(:meeting, @registration.meeting)
    @committee = assign(:committee, @meeting.committee)
  end

  it 'renders new registration form' do
    render

    assert_select 'form[action=?][method=?]', committee_meeting_registrations_path(
      committee_id: @committee.id, meeting_id: @meeting.id
    ), 'post' do
      assert_select 'input#registration_title[name=?]', 'registration[title]'

      assert_select 'textarea#registration_description[name=?]', 'registration[description]'
    end
  end
end
