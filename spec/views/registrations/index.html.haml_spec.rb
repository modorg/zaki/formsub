# frozen_string_literal: true
require 'rails_helper'
include MeetingsHelper

RSpec.describe 'registrations/index', type: :view do
  let(:meeting) { create(:meeting) }
  let(:registration_list) { create_list(:registration, 2, meeting: meeting) }

  before(:each) do
    assign(:registrations, registration_list)
    @meeting = meeting
    @committee = meeting.committee
  end

  it 'renders a list of registrations' do
    render
    registration_list.each do |registration|
      assert_select 'tr>td', text: registration.title
      assert_select 'tr>td', text: registration.case.title
    end
  end
end
