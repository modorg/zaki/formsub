# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'registrations/edit', type: :view do
  before(:each) do
    @registration = assign(:registration, create(:registration))
    @meeting = assign(:meeting, @registration.meeting)
    @committee = assign(:committee, @meeting.committee)
  end

  it 'renders the edit registration form' do
    render

    assert_select 'form[action=?][method=?]', committee_meeting_registration_path(
      @registration, committee_id: @committee.id, meeting_id: @meeting.id
    ), 'post' do
      assert_select 'input#registration_title[name=?]', 'registration[title]'

      assert_select 'textarea#registration_description[name=?]', 'registration[description]'
    end
  end
end
