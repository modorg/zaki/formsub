# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'registrations/show', type: :view do
  before(:each) do
    @registration = assign(:registration, create(:registration))
    @meeting = assign(:meeting, @registration.meeting)
    @committee = assign(:committee, @meeting.committee)
  end

  it 'renders attributes in <p>' do
    render
    expect(rendered).to match(/name/)
    expect(rendered).to match(/info for committee/)
  end
end
