# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'cases/index', type: :view do
  let(:case_list) { create_list(:case, 2) }

  before(:each) do
    assign(:cases, case_list)
  end

  it 'renders a list of cases' do
    render
    case_list.each do |cse|
      assert_select 'tr>td', text: cse.title
      assert_select 'tr>td', text: cse.keyword
    end
  end
end
