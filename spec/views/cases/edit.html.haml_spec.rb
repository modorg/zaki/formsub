# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'cases/edit', type: :view do
  before(:each) do
    @case = assign(:case, create(:case))
  end

  it 'renders the edit case form' do
    render

    assert_select 'form[action=?][method=?]', case_path(@case), 'post' do
      assert_select 'input#case_title[name=?]', 'case[title]'
    end
  end
end
