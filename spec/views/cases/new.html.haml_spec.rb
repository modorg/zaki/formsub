# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'cases/new', type: :view do
  before(:each) do
    assign(:case, build(:case))
    allow(view).to receive(:action_name) { 'new' }
  end

  it 'renders new case form' do
    render

    assert_select 'form[action=?][method=?]', cases_path, 'post' do
      assert_select 'input#case_title[name=?]', 'case[title]'

      assert_select 'input#case_keyword[name=?]', 'case[keyword]'
    end
  end
end
