# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'cases/show', type: :view do
  before(:each) do
    @case = assign(:case, create(:case))
  end

  it 'renders attributes in <p>' do
    render
    expect(rendered).to match(@case.title)
    expect(rendered).to match(@case.keyword)
  end
end
