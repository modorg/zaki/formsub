# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'meetings/index', type: :view do
  let(:meeting_list) { create_list(:meeting, 2) }

  before(:each) do
    assign(:meetings, meeting_list)
    assign(:current_scopes, [])
    @committee = create(:committee)
    @meetings_registrations_count = { meeting_list[0] => 1, meeting_list[1] => 3 }
  end

  it 'renders a list of meetings' do
    render
    meeting_list.each do |meeting|
      assert_select 'tr>td', text: meeting.name
    end
  end
end
