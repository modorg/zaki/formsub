# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'meetings/show', type: :view do
  before(:each) do
    @meeting = assign(:meeting, create(:meeting))
    @committee = assign(:committee, @meeting.committee)
    allow(view).to receive(:params) { { id: @meeting.id } }
  end

  it 'renders attributes in <p>' do
    render
    expect(rendered).to match(@meeting.name)
    expect(rendered).to match(@committee.name)
  end
end
