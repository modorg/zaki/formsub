# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'meetings/edit', type: :view do
  before(:each) do
    @meeting = assign(:meeting, create(:meeting))
    @committee = assign(:committee, create(:committee))
    allow(view).to receive(:params) { { committee_id: @committee.id } }
  end

  it 'renders the edit meeting form' do
    render

    assert_select 'form[action=?][method=?]', committee_meeting_path(@committee, @meeting),
                  'post' do
      assert_select 'input#meeting_name[name=?]', 'meeting[name]'

      assert_select 'input#meeting_approved[name=?]', 'meeting[approved]'
    end
  end
end
