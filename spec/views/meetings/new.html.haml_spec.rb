# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'meetings/new', type: :view do
  before(:each) do
    assign(:meeting, build(:meeting))
    @committee = create(:committee)
  end

  it 'renders new meeting form' do
    render

    assert_select 'form[action=?][method=?]', committee_meetings_path(@committee), 'post' do
      assert_select 'input#meeting_name[name=?]', 'meeting[name]'

      assert_select 'input#meeting_approved[name=?]', 'meeting[approved]'
    end
  end
end
