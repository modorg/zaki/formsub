# frozen_string_literal: true
module Presentable
  extend ActiveSupport::Concern
  # TODO: check if params is permitted, unless raise exception or sth else

  included do
    def self.presenter(params, current_scopes = [])
      @result = all
      @params = params
      @current_scopes = current_scopes

      methods_from_params.each do |method_name|
        return @result.none unless respond_to?(method_name)
        break if @result.empty?
        send(method_name)
      end
      @result
    end

    def self.methods_from_params
      @methods_from_params ||= @params.keys.map { |k| "apply_#{k}" }
    end

    private_class_method :methods_from_params

    # apply methods:

    # rubocop:disable AndOr
    def self.apply_scopes
      @params[:scopes].each do |scope|
        @result = @result.none and break unless respond_to?(scope)
        @current_scopes << scope
        @result = @result.send(scope)
      end
    end
    # rubocop:enable AndOr

    # def self.apply_filters #etc...
    #   # we can add such method following above pattern
    #   # and it should work just fine (no changes to presenter or controller)
    #   # just add method, permit in controller and pass proper http params
    # end
  end
end
