# frozen_string_literal: true
class Meeting < ApplicationRecord
  include Presentable
  # t.string   "name",                              null: false
  # t.integer  "committee_id",                      null: false
  # t.datetime "time_start"
  # t.datetime "time_end"
  # t.boolean  "approved"
  # t.datetime "created_at",                        null: false
  # t.datetime "updated_at",                        null: false
  # t.integer  "max_candidates_number", default: 1, null: false
  # t.text     "agenda"
  # t.integer  "registrations_count",   default: 0
  # t.index ["committee_id"], name: "index_meetings_on_committee_id", using: :btree

  # validations
  validates :name, :committee, :time_start, :time_end, presence: true
  validates :max_candidates_number, numericality: { greater_than: 0 }, presence: true
  validate :max_candidates
  validate :time_end_is_after_start

  # relations
  belongs_to :committee
  has_many :registrations, -> { order :priority_number }, dependent: :restrict_with_exception

  # scopes
  default_scope { order(:time_start) }
  scope :approved, -> { where(approved: true) }
  scope :unapproved, -> { where(approved: false) }

  scope :full, -> { where('max_candidates_number = registrations_count') }
  scope :free, -> { where.not('max_candidates_number = registrations_count') }
  scope :empty, -> { where('registrations_count = 0') }

  scope :future, -> { where('time_start > NOW()') }
  scope :past, -> { where('time_start <= NOW()') }

  # callbacs
  before_validation :set_max_candidates_number, on: :create

  # rest instance methods
  def registration_possible?
    (registrations.size < max_candidates_number) && future?
  end

  def increment_max_candidates_number
    update(max_candidates_number: max_candidates_number + 1)
  end

  def decrement_max_candidates_number
    update(max_candidates_number: max_candidates_number - 1)
  end

  def approve!
    update!(approved: true)
  end

  def generate_agenda
    agenda = registrations.includes(:case).map do |r|
      "#{r.priority_number}. #{r.case&.title || I18n.t(:no_case)}: #{r.title} " \
        "<p>#{r.description}<p>"
    end.join
    update(agenda: agenda)
  end

  def future?
    time_start > Time.zone.now
  end

  private

  def max_candidates
    return if registrations.size <= max_candidates_number
    errors.add(:max_candidates_number, I18n.t(:max_candidates_number_validation_error))
  end

  def set_max_candidates_number
    self.max_candidates_number = (duration_minutes / committee.minutes_per_candidate).to_i
  end

  def duration_minutes
    return 0 if time_start.blank? || time_end.blank?
    ((time_end - time_start) / 60).round
  end

  def time_end_is_after_start
    return if [time_start, time_end].any?(&:blank?)
    errors.add(:time_end, I18n.t(:must_be_after_beginning)) if time_start > time_end
  end
end
