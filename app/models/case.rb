# frozen_string_literal: true
class Case < ApplicationRecord
  extend FriendlyId
  # t.string   "title",      null: false
  # t.string   "keyword",    null: false
  # t.datetime "created_at", null: false
  # t.datetime "updated_at", null: false
  # t.index ["keyword"], name: "index_cases_on_keyword", unique: true, using: :btree

  # validations
  validates :title, presence: true
  validates :keyword, presence: true, uniqueness: true
  validate  :keyword_not_changed

  private

  def keyword_not_changed
    errors.add(:keyword, "can't be changed.") if keyword_changed? && persisted?
  end

  # relations
  has_many :registrations, dependent: :restrict_with_exception

  # callbacks
  after_create :add_id_to_keyword

  # other instance method
  friendly_id :keyword, use: :slugged
  alias_attribute :slug, :keyword

  # rubocop:disable Lint/UselessAccessModifier

  private

  # rubocop:enable Lint/UselessAccessModifier

  def add_id_to_keyword
    # rubocop:disable Rails/SkipsModelValidations
    update_attribute('keyword', "#{keyword}-#{id}")
    # rubocop:enable Rails/SkipsModelValidations
  end
end
