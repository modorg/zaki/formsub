# frozen_string_literal: true
class Registration < ApplicationRecord
  # t.string   "title",           null: false
  # t.text     "description"
  # t.integer  "meeting_id",      null: false
  # t.datetime "created_at",      null: false
  # t.datetime "updated_at",      null: false
  # t.integer  "case_id"
  # t.integer  "priority_number"
  # t.index ["case_id"], name: "index_registrations_on_case_id", using: :btree
  # t.index ["meeting_id"], name: "index_registrations_on_meeting_id", using: :btree

  # validations
  validates :title, presence: true
  validates :meeting, presence: true, on: :create
  validates :case, presence: true, unless: :no_trial?, on: :create
  validates :meeting, uniqueness: { scope: :case }, if: :meeting_and_case?

  validate :max_candidates_for_meeting, on: :create
  validates :priority_number, uniqueness: { scope: :meeting }, if: :meeting

  # relations
  belongs_to :meeting, counter_cache: true
  belongs_to :case, optional: true

  # callbacks
  before_validation :set_priority_number, on: :create

  # rest instance methods
  attr_accessor :case_option

  private

  def max_candidates_for_meeting
    errors.add(:meeting, I18n.t(:registration_not_possible)) unless meeting&.registration_possible?
  end

  def no_trial?
    case_option == 'no_trial'
  end

  def meeting_and_case?
    self.case.present? && meeting.present?
  end

  def set_priority_number
    self.priority_number = (meeting.registrations.reload.last&.priority_number || 0) + 1
  end
end
