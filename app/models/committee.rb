# frozen_string_literal: true
class Committee < ApplicationRecord
  # t.string   "name",                                   null: false
  # t.datetime "created_at",                             null: false
  # t.datetime "updated_at",                             null: false
  # t.string   "zaki_committee_id"
  # t.integer  "minutes_per_candidate",     default: 30
  # t.integer  "default_candidates_number", default: 2
  # t.index ["name"], name: "index_committees_on_name", unique: true, using: :btree

  # validations
  validates :name, presence: true, uniqueness: true
  validates :zaki_committee_id, presence: true
  validates :minutes_per_candidate, :default_candidates_number, presence: true,
                                                                numericality: { only_integer: true }

  # relations
  has_many :meetings, dependent: :restrict_with_exception
  has_many :registrations, through: :meetings, dependent: :restrict_with_exception

  # rest instance methods
  def default_meeting_time
    minutes_per_candidate * default_candidates_number
  end
end
