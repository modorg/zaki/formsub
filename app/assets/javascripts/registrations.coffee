# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

window.form_saver = ->
  if $('#registration_title').val() == ''
    $('#registration_title').val($.cookie('registration_title'))
    if $('#registration_description').val() == ''
      $('#registration_description').val($.cookie('registration_description'))


  $('#registration_title').change ->
    $.cookie('registration_title', this.value)
  $('#registration_description').change ->
    $.cookie('registration_description', this.value)
