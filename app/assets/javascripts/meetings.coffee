# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on 'turbolinks:load', ->
  if $('body').hasClass('new')
    $('#new_meeting input[type=submit]').attr("disabled", true)

  date_pickadate_config = { format: 'yyyy/mm/dd' }
  time_pickadate_config = { format: "HH:i" }
  format = 'YYYY/MM/DD HH:mm'

  $('#day_start').pickadate(date_pickadate_config)
  $('#hour_start').pickatime(time_pickadate_config)

  $('#day_end').pickadate(date_pickadate_config)
  $('#hour_end').pickatime(time_pickadate_config)

  $('#day_start, #hour_start').change ->
    if($('#day_start').val() && $('#hour_start'))
      day_and_hour = $('#day_start').val() + $('#hour_start').val()
      $('#meeting_time_start').val(moment(day_and_hour, format).format(format))
      $('#day_end').val($('#day_start').val())
      $('#hour_end').val(
        moment($('#hour_start').val(), 'HH:mm')
        .add(window.default_meeting_time, 'minutes').format('HH:mm'))
      $('#hour_end').change()

  $('#day_end, #hour_end').change ->
    if($('#day_end').val() && $('#hour_end').val())
      day_and_hour = $('#day_end').val() + $('#hour_end').val()
      $('#meeting_time_end').val(moment(day_and_hour, format).format(format))

  $('#hour_end').change ->
    $('#summary').text(
      moment($('#meeting_time_start').val(), 'YYYY/MM/DD HH:mm').format('llll')\
      + ' - ' +
       moment($('#meeting_time_end').val(), 'YYYY/MM/DD HH:mm').format('lll'))

  $('#hour_end, #meeting_name').on 'focus keyup keypress blur change', ->
    if($('#meeting_name').val() && $('#day_start').val() &&
      $('#hour_start').val() && $('#day_end').val() && $('#hour_end').val())
        $('#new_meeting input[type=submit]').attr("disabled", false)
