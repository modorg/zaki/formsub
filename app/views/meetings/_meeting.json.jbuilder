# frozen_string_literal: true
json.extract! meeting, :id, :name, :committee_id, :time_start, :time_end,
              :approved, :created_at, :updated_at
json.url committee_meeting_url(@committee, meeting, format: :json)
