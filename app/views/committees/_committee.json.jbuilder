# frozen_string_literal: true
json.extract! committee, :id, :name, :created_at, :updated_at
json.meetings committee.meetings.approved.future.select(:id, :name, :time_start, :time_end)
json.url committee_url(committee, format: :json)
