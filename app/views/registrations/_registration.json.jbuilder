# frozen_string_literal: true
json.extract! registration, :id, :title, :description, :meeting_id, :created_at, :updated_at
json.url registration_url(registration, format: :json)
