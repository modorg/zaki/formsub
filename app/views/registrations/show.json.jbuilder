# frozen_string_literal: true
json.partial! 'registrations/registration', registration: @registration
