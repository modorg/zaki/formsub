# frozen_string_literal: true
json.array! @registrations, partial: 'registrations/registration', as: :registration
