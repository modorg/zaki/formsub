# frozen_string_literal: true
json.partial! 'cases/case', cse: @case
