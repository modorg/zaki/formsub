# frozen_string_literal: true
json.extract! cse, :id, :title, :keyword, :created_at, :updated_at
json.registrations do
  json.array! cse.registrations, :id, :title, :description, :meeting, :created_at, :updated_at
end
json.url case_url(cse, format: :json)
