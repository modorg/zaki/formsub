# frozen_string_literal: true
json.array! @cases, partial: 'cases/case', as: :cse
