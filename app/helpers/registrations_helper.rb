# frozen_string_literal: true
module RegistrationsHelper
  def zaki_new_trial_url
    Rails.application.secrets.permitted_keywords_server_url +
      "/committees/#{@committee.zaki_committee_id}/trials/new"
  end

  def no_trial?
    @registration.case_option == 'no_trial'
  end
end
