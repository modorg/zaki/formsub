# frozen_string_literal: true
module MeetingsHelper
  def time_duration(time_start, time_end)
    "#{(time_end - time_start) / 3600}h"
  end

  def increment_max_candidates_number_link(meeting:)
    link_to committee_meeting_increment_max_candidates_number_path(@committee, meeting),
            class: 'btn btn-sm btn-success font-half', 'data-toggle': 'tooltip',
            title: t(:increment_max_candidates_number) do
      yield
    end
  end

  def decrement_max_candidates_number_link(meeting:)
    link_to committee_meeting_decrement_max_candidates_number_path(@committee, meeting),
            class: 'btn btn-sm btn-warning font-half', 'data-toggle': 'tooltip',
            title: t(:decrement_max_candidates_number) do
      yield
    end
  end

  def meeting_registrations_progress_text
    t(:busy_registrations,
      fraction: "#{@meeting.registrations.size} / #{@meeting.max_candidates_number}")
  end

  def filter_message
    msg = @current_scopes.map { |scope| t("mtgs.#{scope}").downcase }.to_sentence.presence
    " (#{msg || t('mtgs.normal').downcase})"
  end
end
