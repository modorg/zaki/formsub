# frozen_string_literal: true
module ApplicationHelper
  def current_user
    # TODO: secure from admin? called on nil value
    return nil unless session_valid?
    refresh_session if session_expired?
    @current_user ||= User.find_by(uid: session[:user_id]['uid'])
                          &.decorate(session[:user_id]['extra'])
  end

  def zaki_committee_url
    # rubocop:disable LineLength
    "#{Rails.application.secrets.permitted_keywords_server_url}/committees/#{@committee.zaki_committee_id}" + (current_user.present? ? '?ensure_login=true' : '')
    # rubocop:enable LineLength
  end

  def meetus_new_meeting_url
    Rails.application.secrets.meetus_url + '/meetings/new'
  end

  def registrations_status
    # for shared/_meeting_bar
    return 0 if @meeting.max_candidates_number.zero?
    100 * @meeting.registrations.size / @meeting.max_candidates_number
  end

  private

  def session_valid?
    session[:user_id].present? && cookies[:sso_session].present?
  end

  def session_expired?
    session_expires_at = session[:user_id]['credentials']['expires_at'].to_s
    session[:user_id] && Time.strptime(session_expires_at, '%s') < Time.zone.now
  end

  def refresh_session
    return if @return_flag.present?
    @return_flag = true

    access_token = session[:user_id]['credentials']['token']
    refresh_token = session[:user_id]['credentials']['refresh_token']

    access_token = OAuth2::AccessToken.new(oauth_client, access_token, refresh_token: refresh_token)

    token_refreshed = access_token.refresh!
    session[:user_id]['credentials'].update(expires_at: token_refreshed.expires_at,
                                            refresh_token: token_refreshed.refresh_token)
  end

  def oauth_client
    ::OAuth2::Client.new(oauth_options.client_id, oauth_options.client_secret,
                         oauth_options.client_options)
  end

  def oauth_options
    @options ||= OmniAuth::Strategies::Sso.default_options.update(
      client_id: Rails.application.secrets.sso_app_id,
      client_secret: Rails.application.secrets.sso_app_secret
    )
  end
end
