# frozen_string_literal: true
class UserSessionsController < ApplicationController
  before_action :login_required, only: [:destroy]

  # omniauth callback method
  #
  # First the callback operation is done
  # inside OmniAuth and then this route is called
  def create
    user = User.find_or_create_by(uid: auth_hash['uid'])
    user.update!(email: auth_hash['info']['email'])

    # Currently storing all the info
    session[:user_id] = auth_hash

    flash[:notice] = t(:successfully_logged_in)
    redirect_to session['sso_redirection'] || params[:origin] || root_path
  end

  def autologin
    redirect_to params[:origin] || root_path if current_user
    @origin = params[:origin]
  end

  # Omniauth failure callback
  def failure
    flash[:notice] = params[:message]
  end

  # logout - Clear our rack session BUT essentially redirect to the provider
  # to clean up the Devise session from there too !
  def destroy
    session[:user_id] = nil

    flash[:notice] = t(:successfully_logged_out)
    redirect_to "#{CUSTOM_PROVIDER_URL}/users/sign_out"
  end

  protected

  def auth_hash
    @auth_hash ||= request.env['omniauth.auth']
  end

end
