# frozen_string_literal: true
class MeetingsController < ApplicationController # rubocop:disable ClassLength
  before_action :set_meeting, only: %w(show edit update destroy increment_max_candidates_number
                                       decrement_max_candidates_number approve edit_agenda
                                       generate_agenda)
  before_action :set_committee
  skip_before_action :login_required, only: [:index, :show]
  before_action :initialize_presenter_helpers, only: :index

  # GET /meetings
  # GET /meetings.json
  def index
    permitted_keywords_get(params[:force_permitted_keyword_get])
    @meetings = if presenter_params.present?
                  @committee.meetings.presenter(presenter_params, @current_scopes)
                else
                  @committee.meetings.future
                end
    @meetings = @meetings.approved unless current_user.present?
  end

  # GET /meetings/1
  # GET /meetings/1.json
  def show; end

  # GET /meetings/new
  def new
    @meeting = Meeting.new
  end

  # GET /meetings/1/edit
  def edit; end

  # POST /meetings
  # POST /meetings.json
  def create
    @meeting = Meeting.new(meeting_params)
    @meeting.committee = @committee
    create_respond_to
  end

  # PATCH/PUT /meetings/1
  # PATCH/PUT /meetings/1.json
  def update
    respond_to do |format|
      if @meeting.update(meeting_params)
        format.html do
          redirect_to [@meeting.committee, @meeting],
                      notice: t(:meeting_successfully_updated)
        end
        format.json { render :show, status: :ok, location: @meeting }
      else
        format.html { render :edit }
        format.json { render json: @meeting.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /meetings/1
  # DELETE /meetings/1.json
  def destroy
    @meeting.destroy
    respond_to do |format|
      format.html do
        redirect_to committee_meetings_url,
                    notice: t(:meeting_successfully_destroyed)
      end
      format.json { head :no_content }
    end
  end

  def increment_max_candidates_number
    @meeting.increment_max_candidates_number
    change_max_candidates_number_redirect(t(:incremented))
  end

  def decrement_max_candidates_number
    @meeting.decrement_max_candidates_number
    change_max_candidates_number_redirect(t(:decremented))
  end

  def approve
    @meeting.approve!
    redirect_to committee_meetings_path(@committee), notice: t(:meeting_approved)
  end

  def edit_agenda; end

  def generate_agenda
    @meeting.generate_agenda
    redirect_to committee_meeting_edit_agenda_path(@committee, @meeting)
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_meeting
    @meeting = Meeting.find(params[:id] || params[:meeting_id])
    login_required unless current_user.present? || @meeting.approved?
  end

  def set_committee
    @committee = Committee.find(params[:committee_id])
  end

  def create_respond_to
    respond_to do |format|
      if @meeting.save
        format.html do
          redirect_to [@meeting.committee, @meeting],
                      notice: t(:meeting_successfully_created)
        end
        format.json { render :show, status: :created, location: @meeting }
      else
        format.html { render :new }
        format.json { render json: @meeting.errors, status: :unprocessable_entity }
      end
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def meeting_params
    params.require(:meeting).permit(:name, :time_start, :time_end, :approved,
                                    :max_candidates_number, :agenda)
  end

  def presenter_params
    params.permit(scopes: [])
  end

  def initialize_presenter_helpers
    # when presentable concern will work with not only scopes we might need more variables here
    @current_scopes = []
  end

  def change_max_candidates_number_redirect(action)
    if @meeting.save
      redirect_to committee_meetings_path(@committee),
                  notice: t(:change_max_candidates_number_success, action: action,
                                                                   meeting_name: @meeting.name)
    else
      redirect_to committee_meetings_path(@committee),
                  alert: t(:change_max_candidates_number_failure, action: action,
                                                                  meeting_name: @meeting.name)
    end
  end
end
