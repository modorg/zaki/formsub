# frozen_string_literal: true
class HomeController < ApplicationController
  skip_before_action :login_required, only: %w(index permitted_keywords_get permitted_keywords_set)

  def index
    redirect_to committees_path if current_user.present?
  end

  def private_index; end

  def permitted_keywords_get
    session[:permitted_keywords_last_parsed_timestamp] = Time.zone.now.to_i
    redirect_to Rails.application.secrets.permitted_keywords_server_url + '/permitted_keywords.json'
  end

  def permitted_keywords_set
    flash[:alert] = params[:alert]
    session[:permitted_keywords] = JSON.parse(URI.unescape(params[:keywords]))
    redirect_to params[:redirect_to_url] || session[:redirect_to_url] || root_path
  end
end
