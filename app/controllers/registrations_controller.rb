# frozen_string_literal: true
class RegistrationsController < ApplicationController
  before_action :set_registration, only: [:show, :edit, :update, :destroy]
  before_action :set_committee, only: [:show, :new, :create, :edit, :index, :update]
  before_action :set_meeting, only: [:show, :new, :create, :edit, :index, :update]
  before_action :set_cases, only: [:new, :create, :edit]
  skip_before_action :login_required, only: [:new, :create]

  # GET /registrations
  # GET /registrations.json
  def index
    if @meeting.present?
      @registrations = @meeting.registrations.includes(:case)
    else
      @meetings_ids = @committee.meetings.future.pluck(:id)
      @registrations = @committee.registrations.where(meeting_id: @meetings_ids)
                                 .includes(:meeting, :case).order(:meeting_id)
    end
  end

  # GET /registrations/1
  # GET /registrations/1.json
  def show; end

  # GET /registrations/new
  def new
    permitted_keywords_get(params[:force_permitted_keyword_get])
    @registration = Registration.new
  end

  # GET /registrations/1/edit
  def edit; end

  # POST /registrations
  # POST /registrations.json
  def create
    @registration = Registration.new(registration_params)
    @registration.meeting = @meeting

    respond_to do |format|
      if @registration.save
        cookies.delete(:registration_title) && cookies.delete(:registration_description)
        format.html do
          redirect_to [@committee, @meeting], notice: t(:registration_successfully_created)
        end
        format.json { render :show, status: :created, location: @registration }
      else
        format.html { render :new }
        format.json { render json: @registration.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /registrations/1
  # PATCH/PUT /registrations/1.json
  def update
    respond_to do |format|
      if @registration.update(registration_params)
        format.html do
          redirect_to [@committee, @meeting, @registration],
                      notice: t(:registration_successfully_updated)
        end
        format.json { render :show, status: :ok, location: @registration }
      else
        format.html { render :edit }
        format.json { render json: @registration.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /registrations/1
  # DELETE /registrations/1.json
  def destroy
    @registration.update(meeting: nil)
    return unless @registration.save

    respond_to do |format|
      format.html do
        redirect_to committee_meeting_registrations_url,
                    notice: t(:registration_successfully_destroyed)
      end
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_registration
    @registration = Registration.find(params[:id])
  end

  def set_committee
    @committee = Committee.find(params[:committee_id])
  end

  def set_meeting
    @meeting = Meeting.find(params[:meeting_id]) if params[:meeting_id].present?
  end

  def set_cases
    @cases = Case.where(keyword: session[:permitted_keywords])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def registration_params
    params.require(:registration).permit(:title, :description, :meeting_id, :case_id, :case_option,
                                         :priority_number)
  end
end
