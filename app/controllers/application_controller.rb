# frozen_string_literal: true
class ApplicationController < ActionController::Base
  include ApplicationHelper

  protect_from_forgery with: :exception, unless: :json_request?
  before_action :authenticate_api, if: :json_request?
  before_action :login_required, unless: :json_request?
  before_action :check_ensure_login

  rescue_from ActiveRecord::DeleteRestrictionError do |exception|
    redirect_to(:back, alert: exception.message)
  end

  rescue_from OAuth2::Error do |_exception|
    session[:user_id] = nil
    redirect_to root_path, alert: I18n.t(:login_error)
  end

  def login_required
    return if current_user
    respond_to do |format|
      format.html do
        session['sso_redirection'] = request.path
        redirect_to "/auth/autologin?#{URI.encode_www_form(origin: request.url)}"
      end
      format.json do
        render json: { 'error' => 'Access Denied' }.to_json
      end
    end
  end

  protected

  def json_request?
    request.format.json?
  end

  def permitted_keywords_get(force = nil)
    session[:redirect_to_url] = request.original_url.split('?').first
    timestamp = session[:permitted_keywords_last_parsed_timestamp]
    return if timestamp.present? && 1.hour.from_now.to_i > timestamp.to_i && force.nil?
    redirect_to get_permitted_keywords_path
  end

  private

  def authenticate_api
    return if request.headers['X-Api-Key'] == Rails.application.secrets.api_key

    head :unauthorized
    false
  end

  def check_ensure_login
    login_required if params[:ensure_login].present?
  end
end
